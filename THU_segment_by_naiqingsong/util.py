#coding=utf-8

__author__ = 'qing'
import numpy as np
import copy
import pandas as pd



def build_synonym_dict():
    synonym_dict, idx = {}, 0
    return synonym_dict, idx
    df = pd.read_excel(u'adv/同义词.xls')
    l = df[u'同义词组'].tolist()

    for line in l:
        for word in line.split(','):
            synonym_dict[word] = idx
        idx += 1

    return synonym_dict, idx


def build_features(df):
    label = 0
    label_dict = {}
    word_dict, idx = build_synonym_dict()
    for index, row in df.iterrows():
        if not row['问题'] in label_dict:
            label_dict[row['问题']] = label
            label += 1
        for word in row['SegmentedInput'].split():
            if not word in word_dict:
                word_dict[word] = idx
                idx += 1
    return word_dict, idx, label_dict


def extract_features(df, label_dict, word_dict, word_count):
    feature, features, labels = [0] * word_count, [], []
    for index, row in df.iterrows():
        if row['问题'] in label_dict:
            labels.append(label_dict[row['问题']])
        else:
            labels.append(0)
        f = copy.deepcopy(feature)
        for word in row['SegmentedInput'].split():
            if word in word_dict:
                f[word_dict[word]] += 1
        features.append(f)

    return np.array(features), np.array(labels)


def build_importance_dict():
    df = pd.read_excel(u'adv/重要词.xls')
    importance_dict = {}
    word_list = df[u'重要词汇'].tolist()
    importance_list = df[u'重要度'].tolist()
    for i in range(len(word_list)):
        if importance_list[i] == u'不重要':
            importance_dict[word_list[i]] = 1
        elif importance_list[i] == u'一般':
            importance_dict[word_list[i]] = 2
        elif importance_list[i] == u'重要':
            importance_dict[word_list[i]] = 3

    return importance_dict


def build_features_with_importance(df):
    label, idx, pos_idx = 0, 0, 0
    label_dict, word_dict = {}, {}
    for index, row in df.iterrows():
        if not row['问题'] in label_dict:
            label_dict[row['问题']] = label
            label += 1
        for word in row['SegmentedInput'].split():
            if not word in word_dict:
                word_dict[word] = idx
                idx += 1

    importance_dict = build_importance_dict()

    return label_dict, word_dict, importance_dict


def extract_features_with_importance(df, label_dict, word_dict, importance_dict):
    feature, features, labels = [0] * len(word_dict), [], []
    for index, row in df.iterrows():
        if row['问题'] in label_dict:
            labels.append(label_dict[row['问题']])
        else:
            labels.append(0)
        f = copy.deepcopy(feature)
        for word in row['SegmentedInput'].split():
            if word in word_dict:
                if word in importance_dict:
                    f[word_dict[word]] += importance_dict[word]
                else:
                    f[word_dict[word]] += 1
        features.append(f)

    #return np.append(np.array(features), np.array(labels), axis=1)
    return np.array(features), np.array(labels)


def extract_features_tune(df):
    label,idx = 0, 0
    label_dict, word_dict, question_dict, input_dict = {}, {}, {}, {}
    for index, row in df.iterrows():
        if not row['问题'] in label_dict:
            label_dict[row['问题']] = label
            question_dict[label] = row['问题']
            label += 1
        for word in row['SegmentedInput'].split():
            item = word.split('_')
            if not item[0] in word_dict:
                word_dict[item[0]] = idx
                idx += 1

    feature, features, labels = [0] * len(word_dict), [], []
    idx = 0
    for index, row in df.iterrows():
        input_dict[idx] = [row['SegmentedInput'], row['SegmentedTarget']]
        labels.append([label_dict[row['问题']], idx])
        f = copy.deepcopy(feature)
        idx += 1
        for word in row['SegmentedInput'].split():
            item = word.split('_')
            if item[0] in word_dict:
                f[word_dict[item[0]]] = f[word_dict[item[0]]] + 2 if item[1]  == 'id' else f[word_dict[item[0]]] + 1
        features.append(f)

    return np.append(np.array(features), np.array(labels), axis=1), label_dict, word_dict, question_dict, input_dict


def build_features_with_pos(df):
    label, idx, pos_idx = 0, 0, 0
    label_dict, word_dict, pos_dict = {}, {}, {}
    for index, row in df.iterrows():
        if not row['问题'] in label_dict:
            label_dict[row['问题']] = label
            label += 1
        for word in row['SegmentedInput'].split():
            item = word.split('_')
            if not item[0] in word_dict:
                word_dict[item[0]] = idx
                idx += 1
            if not item[1] in pos_dict:
                pos_dict[item[1]] = pos_idx
                pos_idx += 1

    return label_dict, word_dict, pos_dict


def extract_features_with_pos(df, label_dict, word_dict, pos_dict):
    feature, features, labels = [0] * len(word_dict), [], []
    for index, row in df.iterrows():
        if row['问题'] in label_dict:
            labels.append(label_dict[row['问题']])
        else:
            labels.append(0)
        f = copy.deepcopy(feature)
        for word in row['SegmentedInput'].split():
            item = word.split('_')
            if item[0] in word_dict:
                f[word_dict[item[0]]] = f[word_dict[item[0]]] + 2 if item[1] == 'n' else f[word_dict[item[0]]] + 1
        features.append(f)

    #return np.append(np.array(features), np.array(labels), axis=1)
    return np.array(features), np.array(labels)



def preprocess_data(input_file):
    df = pd.read_csv(input_file)

    dict = {}
    for index, row in df.iterrows():
        #row['扩展问题'] = row['问题']
        if not row['问题'] in dict:
            dict[row['问题']] = row

    df = df.append(list(dict.itervalues()), ignore_index=True)
    return df

def preprocess_testdata(input_file):
    df = pd.read_csv(input_file)
    return df
