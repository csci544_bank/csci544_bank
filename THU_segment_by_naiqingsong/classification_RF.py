__author__ = 'qing'
import sys
from sklearn.ensemble import RandomForestClassifier
from util import extract_features, build_features
from util import preprocess_data
from util import preprocess_testdata, extract_features_with_pos,build_features_with_pos, extract_features_tune
from sklearn.metrics import accuracy_score
import numpy as np
import pandas as pd


def run(input_file, test_file, k):
    clf = RandomForestClassifier(n_estimators=k)
    df = preprocess_data(input_file)
    word_dict, word_count, label_dict = build_features(df)
    X, y = extract_features(df, label_dict, word_dict, word_count)
    dft = preprocess_testdata(test_file)
    Xt, yt = extract_features(dft, label_dict, word_dict, word_count)
    clf.fit(X, y)
    z = clf.predict(Xt)
    print(accuracy_score(yt, z))


def run_model_with_importance(input_file, test_file, k):
    clf = RandomForestClassifier(n_estimators=k)
    df = preprocess_data(input_file)
    label_dict, word_dict, importance_dict = build_features_with_pos(df)
    X, y = extract_features_with_pos(df, label_dict, word_dict, importance_dict)
    dft = preprocess_testdata(test_file)
    Xt, yt = extract_features_with_pos(dft, label_dict, word_dict, importance_dict)
    clf.fit(X, y)
    z = clf.predict(Xt)
    print(accuracy_score(yt, z))


def tune_rf(input_file, test_file, fold):
    # for k in range(90, 110, 2):
    #     print('k = ' + str(k))
    #     run(input_file, test_file, k)
    df = preprocess_data(input_file)
    X,label_dict, dict, question_dict, input_dict = extract_features_tune(df)
    r, col = X.shape
    c = col - 1
    np.random.shuffle(X)
    row_count = r/fold
    rows, Z = [], []
    for i in range(0, fold - 1):
        rows.append([i * row_count, (i + 1) * row_count - 1])
    rows.append([(fold - 1) * row_count, r - 1])
    accuracy = [0] * fold
    #for cri in ['gini', 'entropy']:
    for max_f in ['sqrt']:
    #for max_f in [None]:
        print("max_features is " + max_f)
        for i in range(0, fold):
            print("round " + str(i))
            clf = RandomForestClassifier(n_estimators=100)
            x1 = np.append(X[0:rows[i][0],:],X[rows[i][1] + 1: r,:],0)
            x2 = X[rows[i][0]: rows[i][1] + 1,: ]
            clf.fit(x1[:,0:c-1], x1[:, c-1])
            z = clf.predict(x2[:,0:c-1])
            accuracy[i] = accuracy_score(x2[:,c - 1], z)
            Z.extend(z)
        print("accuracy is " + str(accuracy[i]))
        print reduce(lambda x, y: x + y, accuracy) / len(accuracy)

    df = pd.DataFrame.from_records([(input_dict[X[i, c]][0], input_dict[X[i, c]][1], question_dict[Z[i]], question_dict[X[i, c-1]]) for i in range(len(Z))])
    df.columns = ['SegmentedInput', 'SegmentedTarget', 'predict', 'label']
    df.to_csv("predict_rf_train.csv", index=False)


if __name__ == "__main__":
    reload(sys)
    sys.setdefaultencoding('utf-8')
    #run("output/training_data_stanford.csv", "output/test_data_stanford.csv", 100)
    #run("test/training_data_thu.csv", "test/test_data_thu.csv", 100)
    print('RF thu pos n')
    run_model_with_importance("test/training_data_thu_pos.csv", "test/test_data_thu_pos.csv", 100)
    #tune_rf("test/training_data_thu_pos.csv", "test/test_data_thu.csv", 5)
    #run("test/training_data_ICTCLAS.csv", "test/ICTCLAS_test.csv", 100)
    #run("test/jieba_seg_train.csv", "test/jieba_seg_test.csv", 100)
    #run("test/dict_training_data_thu.csv", "test/dict_test_data_thu.csv", 100)